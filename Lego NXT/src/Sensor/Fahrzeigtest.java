package Sensor;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class Fahrzeigtest {

	static int eins, zwei, drei, vier, fuenf, sechs, sieben;
	static int[] scans = new int[6];
	static int vergleich;
	static boolean firstTimeCalibrate = true;

	public static void main(String[] args) {

		SensorPort lichtsensorPort = SensorPort.S1;
		LightSensor lichtsensor = new LightSensor(lichtsensorPort);

		/*
		 * 
		 * if (firstTimeCalibrate == true) {
		 * kalibrieren(lichtsensor); firstTimeCalibrate = false; }
		 */
		Button.ENTER.waitForPressAndRelease();
		
		while (true) {
			
			switch (scanPendel(lichtsensor)) {
			case 0:
				Motor.B.stop(true);
				Motor.C.stop(true);
				robibausteine.Strecken.xSekundenbackward(700);
				robibausteine.Strecken.xZeitStellendrehungLinks(1000);
				break;

			case 1:
				Motor.B.stop(true);
				Motor.C.stop(true);
				robibausteine.Strecken.xSekundenbackward(700);
				robibausteine.Strecken.xZeitStellendrehungRechts(1000);
				break;
			case 2:
				robibausteine.Strecken.xSekundenForward(900);

			default:
			}
		}
		/*
		 * while (scanPendel(lichtsensor)) {
		 * 
		 * Motor.B.forward(); Motor.C.forward();
		 * 
		 * }
		 * 
		 * while (!scanPendel(lichtsensor)) {
		 * 
		 * Motor.B.stop(true); Motor.C.stop(true);
		 * 
		 * robibausteine.Strecken.xSekundenbackward(1000);
		 * robibausteine.Strecken.xZeitStellendrehungLinks(1000); }
		 */
	}

	public static void kalibrieren(LightSensor licht) {

		String hellString = "" + licht.readValue();
		LCD.drawString(hellString, 1, 3);

		Button.ENTER.waitForPressAndRelease();

		String dunkelString = "" + licht.readValue();
		LCD.drawString(dunkelString, 1, 4);
	}

	public static int scanPendel(LightSensor licht) {

		defaultPosition();
		Motor.A.setSpeed(29);
		for (int i = 0; i < scans.length; i++) {
			scans[i] = licht.readValue();
			LCD.drawInt(scans[i], 1, i + 1);
			robibausteine.Strecken.xSekundenbackwardScanner(1000);
		}

		defaultPosition2();

		return vergleich();

	}

	public static void defaultPosition() {
		Motor.A.setSpeed(80);

		Motor.A.forward();

		try {
			Thread.sleep(950);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.A.stop();

	}

	public static void defaultPosition2() {
		Motor.A.setSpeed(100);

		Motor.A.forward();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.A.stop();

	}

	public static int vergleich() {

		for (int i = 0; i < scans.length; i++) {
			if (scans[i] > 33) {
				vergleich = 2;
			}
		}

		if ((scans[0] <= 30) || (scans[1] <= 30) || (scans[2] <= 30))
			return 1;

		if ((scans[3] <= 30) || (scans[4] <= 30) || (scans[5] <= 30))
			return 0;

		return vergleich;

	}
}
