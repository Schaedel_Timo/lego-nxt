package needForSpeed;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class RundenFahren {

	static boolean k;
	static int f, g;
	static int r, l = 0;
	static int x = 25;

	public static void main(String[] args) {

		SensorPort lichtsensorPort = SensorPort.S1;
		LightSensor lichtsensor = new LightSensor(lichtsensorPort);

		Button.ENTER.waitForPressAndRelease();

		while (true) {
			driveAlgo(lichtsensor);
		}

	}

	public static void driveAlgo(LightSensor lichtsensor) {
		Motor.B.setSpeed(400);
		Motor.C.setSpeed(400);

		Motor.B.forward();
		Motor.C.forward();

		while ((lichtsensor.readValue() >= 45) && (r > l)) { // wei�
			rechts(lichtsensor);
		}
		
		while ((lichtsensor.readValue() >= 45) && (r == l)) { // wei�
			links(lichtsensor);
		}

		while ((lichtsensor.readValue() >= 45) && (r < l)) { // wei�
			links(lichtsensor);
		}

	}

	public static void rechts(LightSensor lichtsensor) {

		Motor.B.setSpeed(100);
		Motor.C.setSpeed(100);
		k = true;
		f = 0;
		robibausteine.Strecken.rechts();

		while (k) {

			if (f == 100) {
				k = false;
			}
			if (lichtsensor.readValue() < 36) { // schwarz
				r += 1;
				LCD.drawInt(r, 0, 0);
				
				break;
			}

			try {
				Thread.sleep(7);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			f += 1;
		}

		if (lichtsensor.readValue() >= 45)linksvonRechts(lichtsensor);

	}

	public static void linksvonRechts(LightSensor lichtsensor) {

		robibausteine.Strecken.links();
		while (true) {
			if (lichtsensor.readValue() < 36) { // schwarz
				l+=1;
				LCD.drawInt(l, 1, 1);
				
				break;
			}
		}

	}

	public static void links(LightSensor lichtsensor) {

		Motor.B.setSpeed(100);
		Motor.C.setSpeed(100);
		k = true;
		f = 0;
		robibausteine.Strecken.links();

		while (k) {

			if (f == 100) {
				k = false;
			}
			if (lichtsensor.readValue() < 36) { // schwarz
				l += 1;
				LCD.drawInt(l, 1, 1);
				
				break;
			}

			try {
				Thread.sleep(7);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			f += 1;
		}

		if (lichtsensor.readValue() >= 45)rechtsvonLinks(lichtsensor);

	}

	public static void rechtsvonLinks(LightSensor lichtsensor) {

		robibausteine.Strecken.rechts();
		while (true) {
			if (lichtsensor.readValue() < 36) { // schwarz
				r+=1;
				LCD.drawInt(r, 0, 0);
				
				break;
			}
		}

	}
}
