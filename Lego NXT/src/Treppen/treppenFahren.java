package Treppen;

import java.io.File;

import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.TouchSensor;

public class treppenFahren {

	

	public static void main(String[] args) {
		Button.ENTER.waitForPressAndRelease();
		TouchSensor touch = new TouchSensor(SensorPort.S1);
		
		
		while (true) {
			fahre(touch);
		}
	}

	public static void fahre(TouchSensor touch) {
		robibausteine.Strecken.backwards();
		
		
		
		if (touch.isPressed()) {
			robibausteine.Strecken.xSekundenForward(200);
			Motor.B.setSpeed(10000);
			Motor.C.setSpeed(1000);
			
			robibausteine.Strecken.xSekundenbackward(400);
			robibausteine.Strecken.stop();
			
			
		}
	}
}
