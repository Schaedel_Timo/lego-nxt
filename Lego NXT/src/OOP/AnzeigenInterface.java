package OOP;

public interface AnzeigenInterface {

	public void zeigeText(String text);
	
	public void zeigeZahl(float zahl);
}
