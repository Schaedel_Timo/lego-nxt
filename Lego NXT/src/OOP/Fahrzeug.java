package OOP;

import lejos.nxt.Button;
import lejos.nxt.Motor;

public class Fahrzeug implements FahrzeugInterface, AnzeigenInterface {

	double pi = 3.141;

	@Override
	public float getDurchmesser() {

		return 0;
	}

	@Override
	public float getAbstandAchse() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getGeschwindigkeitLinks() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getGeschwindigkeitRechts() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setGeschwindigkeit(int links, int rechts) {
		Motor.B.setSpeed(links);
		Motor.C.setSpeed(rechts);

	}

	@Override
	public void vorwaerts(int grad) {

		Motor.C.forward();

		try {
			int zeit = (int) ((2 * Math.PI * 12) * ((grad / 360.0) / 17.6) * 970 * (360.0/Motor.C.getSpeed()));

			Thread.sleep(zeit);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.C.stop(true);

	}

	@Override
	public void vorwaertsStrecke(int strecke) {

		Motor.B.forward();
		Motor.C.forward();
		try {
			Thread.sleep((long) ((strecke / 17.6) * 1000 * (360.0 / Motor.B.getSpeed())));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);

	}

	@Override
	public void vorwaertsZeit(int zeit) {
		Motor.B.setSpeed(180);
		Motor.C.setSpeed(180);

		Motor.B.forward();
		Motor.C.forward();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.setSpeed(220);
		Motor.C.setSpeed(220);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.setSpeed(250);
		Motor.C.setSpeed(250);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.setSpeed(300);
		Motor.C.setSpeed(300);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.setSpeed(360);
		Motor.C.setSpeed(360);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.setSpeed(420);
		Motor.C.setSpeed(420);

		Motor.B.stop(true);
		Motor.C.stop(true);

	}

	@Override
	public void rueckwaerts() {
		// TODO Auto-generated method stub

	}

	@Override
	public void rueckwaertsStrecke(int Srecke) {
		// TODO Auto-generated method stub

	}

	@Override
	public void rueckwaertsZeit(int zeit) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.setSpeed(30);
		Motor.C.setSpeed(30);

		Motor.B.backward();
		Motor.C.backward();

		try {
			Thread.sleep(zeit);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop();
		Motor.C.stop();
	}

	@Override
	public void drehRechts() {
		// TODO Auto-generated method stub

	}

	@Override
	public void drehRechtsGrad(int grad) {

		Motor.C.forward();

		try {
			int zeit = (int) ((2 * Math.PI * 12) * ((grad / 360.0) / 17.6) * 800 * (360.0/Motor.C.getSpeed()));

			Thread.sleep(zeit);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.C.stop(true);

	}

	@Override
	public void drehRechtsZeit(int zeit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drehLinks() {
		// TODO Auto-generated method stub

	}

	@Override
	public void drehLinksGrad(int grad) {

		Motor.B.setSpeed(180);
		Motor.B.forward();

		try {
			int zeit = (int) (((2 * Math.PI * 12) / 17.6 * 2000) * ((grad-2) / 360.0));

			Thread.sleep(zeit);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);

	}

	@Override
	public void drehLinksZeit(int zeit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

	@Override
	public void zeigeText(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void zeigeZahl(float zahl) {
		// TODO Auto-generated method stub

	}

}
