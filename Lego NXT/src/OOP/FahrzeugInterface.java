package OOP;

public interface FahrzeugInterface{

	
	public float getDurchmesser();
	
	public float getAbstandAchse();
	
	public int getGeschwindigkeitLinks();
	
	public int getGeschwindigkeitRechts();
	
	public void setGeschwindigkeit(int links, int rechts);
	
	public void vorwaerts(int strecke);
	
	public void vorwaertsStrecke(int strecke);
	
	public void vorwaertsZeit(int zeit);
	
	public void rueckwaerts();
	
	public void rueckwaertsStrecke(int Srecke);
	
	public void rueckwaertsZeit(int zeit);
	
	public void drehRechts();
	
	public void drehRechtsGrad(int grad);

	public void drehRechtsZeit(int zeit);
	
	public void drehLinks();
	
	public void drehLinksGrad(int grad);
	
	public void drehLinksZeit(int zeit);
	
	public void stop();
	
}
