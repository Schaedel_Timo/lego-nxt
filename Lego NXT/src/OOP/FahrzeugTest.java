/**
   description Tesklasse zur Demonstration der N�tzlickeit von
               Interfaces

  @author      Ehlert / Hafezi / G�kcen OSZ IMT
  @version     1.0 / 08.10.2008
  @version     1.1 / 22.01.2013
*/
package OOP;

import lejos.nxt.Button;
import lejos.nxt.Motor;

public class FahrzeugTest {

	// Anfang Attribute
	// Ende Attribute

	// Anfang Methoden
	public static void main(String[] args) {

		Fahrzeug einFahrzeug = new Fahrzeug();
		// Fahrzeugnamen �ndern
		// Speed-Default-Wert ist 360?!
		Motor.B.setSpeed(180);
		Motor.C.setSpeed(180);

		// 1. Aufgabe
		// ????

	
		Button.ENTER.waitForPressAndRelease();
		for (int i = 1; i <= 2; i++) {
			einFahrzeug.vorwaertsStrecke(50);
			einFahrzeug.vorwaerts(90);
			einFahrzeug.vorwaertsStrecke(20);
			einFahrzeug.vorwaerts(90);
		}

		Button.ENTER.waitForPressAndRelease();
		// 2. Aufgabe
		// ????
		for (int i = 1; i <= 36; i++) {
			einFahrzeug.vorwaertsStrecke(5);
			einFahrzeug.drehLinksGrad(10);
		}

		Button.ENTER.waitForPressAndRelease();
		// 3. Aufgabe
		einFahrzeug.setGeschwindigkeit(600, 600);
		// ????
		for (int i = 1; i <= 2; i++) {
			einFahrzeug.vorwaertsStrecke(50);
			einFahrzeug.drehRechtsGrad(90);
			einFahrzeug.vorwaertsStrecke(20);
			einFahrzeug.drehRechtsGrad(90);
		}

		Button.ENTER.waitForPressAndRelease();
		// 4. Aufgabe
		// Fahrzeug f�hrt 10s vorw�rts, dabei "beschleunigt" es,
		// anschlie�end bleibt es 2s stehen und dann f�hrt es mit
		// kleinster Geschwindigkeit 5s r�ckw�rts
		
		einFahrzeug.vorwaertsZeit(10000);
		einFahrzeug.rueckwaertsZeit(5000);

	}
	// Ende Methoden

}
