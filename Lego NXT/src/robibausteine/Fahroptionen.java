package robibausteine;
import java.util.Random;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.MotorPort;
import lejos.robotics.RegulatedMotor;

public class Fahroptionen {

	public static void main(String[] args) {

		//geradeaus();
	//	rueckwaerts();
	//	rechtsdrehung();
	//	quadrat();
	//	fuenfSekunde();
		einMeter();
	//	kreis();
		random();
		
	}
	
	public static void geradeaus() {
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("Vorw�rts", 3, 1);
		LCD.refresh();
		
		Strecken.forwards();
	}
	
	public static void rueckwaerts() {
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("R�ckw�rts", 3, 2);
		LCD.refresh();
		
		Strecken.backwards();
	}
	
	public static void rechtsdrehung() {
		
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("drehung 90�", 3, 3);
		LCD.refresh();
		
		Strecken.forwards();
		Strecken.rechts90();
		
	}
	
	public static void quadrat() {
		
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("Quadrat", 3, 4);
		LCD.refresh();
		
		for (int i = 0; i < 4; i++) {
			Strecken.lowForwards();
			Strecken.rechts90();
		}
			
	}
	
	public static void fuenfSekunde() {
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("eine Sekunde", 3, 5);
		LCD.refresh();
		
		Motor.B.setSpeed(100);
		Motor.C.setSpeed(100);
		Motor.B.forward();
		Motor.C.forward();
		

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		
		Motor.B.stop(true);
		Motor.C.stop(true);
	}
	
	public static void einMeter() {

		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("ein Meter", 3, 6);
		LCD.refresh();
		
		Strecken.einMeterFahren();
	}
	
	public static void kreisLinks() {
		
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("Kreis links", 3, 7);
		LCD.refresh();
		
		Motor.B.setSpeed(360);
		Motor.C.setSpeed(180);
		Motor.B.forward();
		Motor.C.forward();
		

			try {
				Thread.sleep(8400);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		
		Motor.B.stop(true);
		Motor.C.stop(true);	
		
	}

	public static void kreisRechts() {
		
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("Kreis rechts", 3, 7);
		LCD.refresh();
		
		Motor.B.setSpeed(180);
		Motor.C.setSpeed(360);
		Motor.B.forward();
		Motor.C.forward();
		

			try {
				Thread.sleep(8400);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		
		Motor.B.stop(true);
		Motor.C.stop(true);	
		
	}
	
	
	public static void random() {
		
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("random", 3, 8);
		LCD.refresh();
		Random rand = new Random();
		int zahl = rand.nextInt(2);
		
		Strecken.einMeterFahren();
		
		if (zahl == 0) {
			Strecken.rechts90();
		} else if (zahl == 1) {
			Strecken.links90();
		}
		
		Strecken.einMeterFahren();
	}
}
