package robibausteine;

import lejos.nxt.LightSensor;
import lejos.nxt.Motor;

public class Strecken {

	public static void einMeterFahren() {

		Motor.B.setSpeed(360);
		Motor.C.setSpeed(360);
		Motor.B.forward();
		Motor.C.forward();

		try {
			Thread.sleep(5790);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);
	}

	public static void fahrenUndRechts() {

		Motor.B.setSpeed(360);
		Motor.C.setSpeed(360);
		Motor.B.forward();
		Motor.C.forward();

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);

		Motor.B.forward();

		try {
			Thread.sleep(1250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop();
	}

	public static void backwards() {


		Motor.B.backward();
		Motor.C.backward();



	}

	public static void forwards() {

		Motor.B.forward();
		Motor.C.forward();
	}

	public static void rechts90() {

		Motor.B.setSpeed(360);
		Motor.B.forward();

		try {
			Thread.sleep(1040);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop();
	}

	public static void links90() {

		Motor.C.setSpeed(360);
		Motor.C.forward();

		try {
			Thread.sleep(1040);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.C.stop();
	}

	public static void lowForwards() {
		Motor.B.setSpeed(360);
		Motor.C.setSpeed(360);
		Motor.B.forward();
		Motor.C.forward();

		try {
			Thread.sleep(1250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);
	}

	public static void xZeitStellendrehungRechts(Integer x) {

		Motor.B.forward();
		Motor.C.backward();

		try {
			Thread.sleep(x);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);
	}

	public static void xZeitStellendrehungLinks(Integer x) {

		Motor.C.forward();
		Motor.B.backward();

		try {
			Thread.sleep(x);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);
	}
	
	public static void xSekundenForward(Integer x) {

		Motor.C.forward();
		Motor.B.forward();

		try {
			Thread.sleep(x);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);
		
		
	}
	
	public static void xSekundenbackward(Integer x) {

		Motor.B.backward();
		Motor.C.backward();

		try {
			Thread.sleep(x);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.B.stop(true);
		Motor.C.stop(true);
		
		
	}
	
	public static void xSekundenbackwardScanner(Integer x) {

		Motor.A.backward();
		

		try {
			Thread.sleep(x);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Motor.A.stop(true);
		
		
		
	}
	
	public static boolean lightSensor(LightSensor licht) {
		
		if (licht.readValue() > 36) {
			return true;
		}
		
		return false;
		
		
	}
	
	public static void rechts() {

		Motor.B.forward();
		Motor.C.backward();

	}
	
	public static void links() {

		Motor.C.forward();
		Motor.B.backward();

	}
	
	public static void stop() {
		
		Motor.C.stop(true);
		Motor.B.stop(true);
	}


	

}
