package dance;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.MotorPort;
import lejos.robotics.RegulatedMotor;

public class backgroundDancer {

	public static void main(String[] args) {

		Button.ENTER.waitForPressAndRelease();
		for (int i = 0; i < 7; i++) {
			robibausteine.Strecken.rechts90();
			robibausteine.Strecken.xSekundenForward(1000);
			robibausteine.Strecken.xSekundenbackward(1100);
		}
		
		robibausteine.Strecken.xZeitStellendrehungLinks(10000);
		robibausteine.Strecken.xZeitStellendrehungRechts(5000);
		robibausteine.Strecken.xZeitStellendrehungLinks(5000);
	}

}
