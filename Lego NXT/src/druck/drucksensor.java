package druck;

import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class drucksensor implements Druck {

	public static void main(String[] args) {
		
		drucksensor ds = new drucksensor();
		while (true) {
			
			if (ds.pressed()) {
				LCD.drawString("AUA!",0, 0);
			}
			
		}
	}

	@Override
	public boolean pressed() {
		TouchSensor touch = new TouchSensor(SensorPort.S1);

		return touch.isPressed();
	}

}
